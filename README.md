Anahata Wellness Resort, Koh Samui is more than just a resort, it’s a place to totally RETREAT from the stress of modern day life. We are situated at the beachfront of Lipanoi Beach, one of the quietest and most unspoiled western shores of Koh Samui.

Address: 95/70 Moo 2, Lipa Noi, Koh Samui, Surat Thani 84140, Thailand

Phone: +66 77 485 775

Website: https://anahatasamui.com
